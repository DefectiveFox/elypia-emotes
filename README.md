# Elypia Emotes [![discord-members]][discord] [![gitlab-build]][gitlab]
All emotes for Elypia which are open-source and available via the Apache 2.0 license.  
For information on contributing please check out [the contribution guide]. 

## Download
You can download all emotes [here]!  

---

Sometimes GitLab Pages isn't up to date with the repo, if there's an emote you want but you can't find it in the archive,
feel free to [download] the artifacts from the latest pipeline instead, it should be available there!

## Emotes
### All Colors
![All Colors]

### All Emotes
![All Emotes]

[discord]: https://discord.gg/hprGMaM "Discord Invite"
[gitlab]: https://gitlab.com/Elypia/elypia-emotes/commits/master "Repository on GitLab"
[the contribution guide]: ./CONTRIBUTING.md "Contribute to the Elypia Emotes"
[here]: https://elypia.gitlab.io/elypia-emotes/emotes.zip "All Emotes Packaged"
[download]: https://gitlab.com/Elypia/elypia-emotes/-/jobs/artifacts/master/download?job=pages "Download Latest Pipeline"

[discord-members]: https://discordapp.com/api/guilds/184657525990359041/widget.png "Discord Shield"
[gitlab-build]: https://gitlab.com/Elypia/elypia-emotes/badges/master/pipeline.svg "GitLab Build Shield"
[All Colors]: https://elypia.gitlab.io/elypia-emotes/colors.png "All Colors"
[All Emotes]: https://elypia.gitlab.io/elypia-emotes/emotes.png "All Unique Emotes"